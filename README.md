#1 编译及开发环境

* jdk 1.8 +
* maven 3.3 +

#2 编译方法

执行maven命令编译：

``` shell
mvn clean package
```

编译产物位于`target/psum-0.0.1-SNAPSHOT.jar`

#3 运行

运行前可用以下脚本生成测试文件：

``` shell
seq -f '%1.f' 1 1000000 > test.txt
```

然后执行以下命令：

``` shell
java -jar target/psum-0.0.2-SNAMSHOT.jar test.txt
```

还可指定多线程时使用的线程数，如下：

*不指定时，默认为机器Core数加1。*

``` shell
java -jar target/psum-0.0.2-SNAMSHOT.jar test.txt 10
```

下面是我在mac上执行的结果：

``` plaintext
File loading elapsed 119.210659 ms.
Serial sum elapsed 4.242731 ms.
Serial sum result: 500000500000.
Parallel sum elapsed 8.364891 ms.
Parallel sum result: 500000500000.
```

