/*
 * Created by Yan Jian on 2017/6/24.
 * Email: yanjian_cn@163.com
 */
package com.rosexu.sum.core;

import com.rosexu.sum.PSumApp;
import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.assertTrue;

public class SummerTest {

    private static final int LENGTH = 100000;

    @Test
    public void test() {
        int[] data = new int[LENGTH];
        Arrays.fill(data, Integer.MAX_VALUE);
        long s = Summer.serialSum(data, 0, LENGTH);
        long p = Summer.parallelSum(data, 0, LENGTH, PSumApp.DEFAULT_NUM_THREADS);
        // System.out.println(s + "\t" + p);
        assertTrue(s == p);
    }
}
