/*
 * Created by Yan Jian on 2017/6/23.
 * Email: yanjian_cn@163.com
 */
package com.rosexu.sum.core;

public final class Timer {
    private final String name;
    private long startNano;
    private long endNano;

    private Timer(String name) {
        this.name = name;
    }

    public static Timer timing(String procName) {
        Timer timer = new Timer(procName);
        timer.begin();
        return timer;
    }

    public void finish() {
        end();
        printMsg();
    }

    private void begin() {
        this.startNano = System.nanoTime();
    }

    private void end() {
        this.endNano = System.nanoTime();
    }

    private void printMsg() {
        System.out.println(this);
    }

    @Override
    public String toString() {
        return name + " elapsed " + elapsedMs() + " ms.";
    }

    private double elapsedMs() {
        long psumElapsed = endNano - startNano;
        return psumElapsed / 1.0e6;
    }
}
