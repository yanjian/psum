/*
 * Created by Yan Jian on 2017/6/23.
 * Email: yanjian_cn@163.com
 */
package com.rosexu.sum.core;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * 求和器
 */
public final class Summer {

    private final int[] intArray;
    private final int begin;
    private final int end;
    private final AtomicInteger completeCounter;
    private final int totalTasks;

    public Summer(int[] intArray, int begin, int end, int totalTasks) {
        assert intArray != null;
        assert begin >= 0;
        assert end <= intArray.length;
        assert begin < end;
        assert totalTasks > 1;
        this.intArray = intArray;
        this.begin = begin;
        this.end = end;
        this.completeCounter = new AtomicInteger();
        this.totalTasks = totalTasks;
    }

    public static long serialSum(int[] data, int begin, int end) {
        long agg = 0L;
        for (int i = begin; i < end; i++) {
            agg += data[i];
        }
        return agg;
    }

    public static long parallelSum(int[] data, int begin, int end, int nThreads) {
        Summer summer = new Summer(data, begin, end, nThreads);
        return summer.sum();
    }

    public long sum() {
        SumTask[] tasks = split();
        startMapTasks(tasks);
        blockUntilComplete();
        return reduce(tasks);
    }

    private SumTask[] split() {
        assert end - begin > totalTasks;
        int totalSize = end - begin;
        int partSize = totalSize / totalTasks;
        if (totalSize % totalTasks != 0) {
            partSize += 1;
        }
        SumTask[] tasks = new SumTask[totalTasks];
        for (int i = 0; i < totalTasks; i++) {
            int partStart = i * partSize;
            int partEnd = Integer.min((i + 1) * partSize, end);
            tasks[i] = new SumTask(intArray, partStart, partEnd);
        }
        return tasks;
    }

    private void blockUntilComplete() {
        synchronized (this) {
            try {
                while (completeCounter.get() < totalTasks) {
                    wait();
                }
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }

    private static void startMapTasks(SumTask[] tasks) {
        for (SumTask task : tasks) {
            task.start();
        }
    }

    private static long reduce(SumTask[] tasks) {
        long sum = 0L;
        for (SumTask task : tasks) {
            sum += task.getResult();
        }
        return sum;
    }

    public final class SumTask extends Thread {
        private final int[] data;
        private final int begin, end;
        private long result;
        private Exception exception;

        private SumTask(int[] data, int begin, int end) {
            this.data = data;
            this.begin = begin;
            this.end = end;
            exception = null;
        }

        @Override
        public void run() {
            try {
                result = serialSum(data, begin, end);
            } catch (Exception e) {
                exception = e;
            }
            if (completeCounter.incrementAndGet() == totalTasks) {
                synchronized (Summer.this) {
                    Summer.this.notify();
                }
            }
        }

        public long getResult() {
            if (exception != null) {
                throw new RuntimeException(exception);
            }
            return result;
        }

    }
}
