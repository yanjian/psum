/*
 * Created by Yan Jian on 2017/6/23.
 * Email: yanjian_cn@163.com
 */
package com.rosexu.sum.core;

import java.io.BufferedReader;
import java.io.Closeable;
import java.io.FileReader;
import java.io.IOException;

public class DataLoader {

    private String fileName;

    private DataLoader() {
    }

    public static DataLoader newReader() {
        return new DataLoader();
    }

    public DataLoader withFile(String fileName) {
        this.fileName = fileName;
        return this;
    }

    public int loadTo(final int[] buffer) {
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader(fileName));
            String lineText = null;
            int lineIndex = 0;
            while (lineIndex < buffer.length && (lineText = reader.readLine()) != null) {
                buffer[lineIndex++] = Integer.parseInt(lineText);
            }
            return lineIndex;
        } catch (IOException | NumberFormatException e) {
            e.printStackTrace();
            return 0;
        } finally {
            closeQuietly(reader);
        }
    }

    private static void closeQuietly(Closeable comp) {
        if (comp == null) {
            return;
        }
        try {
            comp.close();
        } catch (IOException e) {
            // ignore
        }
    }
}
