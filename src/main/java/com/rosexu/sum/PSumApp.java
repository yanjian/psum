/*
 * Created by Yan Jian on 2017/6/23.
 * Email: yanjian_cn@163.com
 */
package com.rosexu.sum;

import com.rosexu.sum.core.DataLoader;
import com.rosexu.sum.core.Summer;
import com.rosexu.sum.core.Timer;

public class PSumApp {
    public static final int DEFAULT_NUM_THREADS = Runtime.getRuntime()
            .availableProcessors() + 1;
    private static final int MAX_LINES = 1000000;

    public static void main(String[] args) throws InterruptedException {
        if (args.length < 1) {
            System.err.println("You should an argument to specify a file to read.");
            System.exit(1);
        }
        final String filePath = args[0];

        int nThreads = DEFAULT_NUM_THREADS;
        if (args.length == 2) {
            try {
                int n = Integer.parseInt(args[1]);
                if (n > 1) {
                    nThreads = n;
                }
            } catch (NumberFormatException e) {
                // ignore
            }
        }

        final int[] buffer = new int[MAX_LINES];

        Timer loadingTimer = Timer.timing("File loading");
        int count = DataLoader.newReader()
                .withFile(filePath)
                .loadTo(buffer);
        loadingTimer.finish();

        Timer serialSumTimer = Timer.timing("Serial sum");
        long sum = Summer.serialSum(buffer, 0, count);
        serialSumTimer.finish();
        System.out.println("Serial sum result: " + sum + ".");

        Timer parallelSumTimer = Timer.timing("Parallel sum");
        long psum = Summer.parallelSum(buffer, 0, count, nThreads);
        parallelSumTimer.finish();
        System.out.println("Parallel sum result: " + psum + ".");
    }

}
